package com.training.bookorderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BookorderserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookorderserviceApplication.class, args);
	}

}
