package com.training.bookorderservice.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.training.bookorderservice.model.BookOrder;


@Service
public interface BookOrderService {

	List<BookOrder> getAllBookOrders();

	BookOrder getOneBookOrder(long id);

	

	

	BookOrder getBookOrderByBookId(long bookId);

	BookOrder createBookOrder(BookOrder bookorder);

	BookOrder updateBookOrder(BookOrder bookorder);

	boolean deleteBookOrder(long bookid);

	
	
}
