package com.training.bookorderservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.bookorderservice.model.BookOrder;
import com.training.bookorderservice.repository.BookOrderRepository;


@Service
public class BookOrderServiceImpl implements BookOrderService {

	@Autowired
	BookOrderRepository repository;

	@Override
	public List<BookOrder> getAllBookOrders() {
	return repository.findAll();
	}
	


	

	@Override
	public BookOrder getOneBookOrder(long id) {
		return repository.findById(id).orElse(new BookOrder());	
	}
	@Override
	public BookOrder getBookOrderByBookId(long bookId) {
		return repository.getBookOrderByBookId(bookId);
	}
	@Override
	public BookOrder createBookOrder(BookOrder bookorder) {
		return repository.save(bookorder);
	}
	@Override
	public BookOrder updateBookOrder(BookOrder bookorder) {
		return repository.save(bookorder);
		
	}


	@Override
	public boolean deleteBookOrder(long bookid) {
		repository.deleteById(bookid);
		return true;
		}
	
	
	
	
	
	
	
	
}

