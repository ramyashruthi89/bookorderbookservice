package com.training.bookorderservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.bookorderservice.model.BookOrder;


@Repository
public interface BookOrderRepository extends JpaRepository<BookOrder, Long> {

	BookOrder getBookOrderByBookId(long bookId);

	
}
