 package com.training.bookorderservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.training.bookorderservice.model.BookOrder;
import com.training.bookorderservice.service.BookOrderService;




@RestController
public class BookOrderController {

	@Autowired
	BookOrderService bookorderService;

	// CRUD
	// RequestMapping
	// Create --> PostMapping - completed
	// Read One--> GetMapping - completed
	// Read All--> GetMapping - completed
	// Update --> PutMapping - completed
	// Delete --> DeleteMapping - completed

	@GetMapping(value = "/bookorders")
	public List<BookOrder> getAllBookOrders() {
		return bookorderService.getAllBookOrders();
	}

	@GetMapping(value = "/bookorder")
	//http://localhost:4321/bookorder?id=1
		public BookOrder getOneBookOrder(@RequestParam long id) {
		return  bookorderService.getOneBookOrder(id);
	}

	@GetMapping(value = "/bookorder/{bookId}")
	// http://localhost:4321/bookorder/1
	
	// do not give too many path variables
	public BookOrder getBookOrderByBookId(@PathVariable long bookId) {
		System.out.println("I am invoked for bookId:" + bookId);
		return bookorderService.getBookOrderByBookId(bookId);
	}

	@PostMapping(value = "/bookorder")
	public BookOrder createBookOrder(@RequestBody BookOrder bookorder) {
		return bookorderService.createBookOrder(bookorder);
	}

	@PutMapping(value = "/bookorder")
	public BookOrder updateBookOrder(@RequestBody BookOrder bookorder) {
		System.out.println("Update the book:" + bookorder);
		return bookorderService.updateBookOrder(bookorder);
	}

	@DeleteMapping(value = "/bookorder/{bookid}")
	public String deleteBookOrderById(@PathVariable long bookid) {
		try {
			if (true) { 
				bookorderService.deleteBookOrder(bookid);
				return "Deleted Successfully";
			}
		} catch (Exception e) {
		}
		return "Cannot able to delete the BookOrder";

	}
}
