package com.training.bookorderservice.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookOrder {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long orderId;
	private long bookId;
	private int totalPrice;
	private Date orderDate;
	

}
