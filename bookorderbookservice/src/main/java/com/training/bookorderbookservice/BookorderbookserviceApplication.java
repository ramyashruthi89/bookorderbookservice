package com.training.bookorderbookservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class BookorderbookserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookorderbookserviceApplication.class, args);
	}

}
