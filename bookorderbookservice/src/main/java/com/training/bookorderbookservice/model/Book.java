package com.training.bookorderbookservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Book {
	private long bookId;
	private String bookName;
	private float bookPrice;
	private int bookQty;
	private BookOrder bookorder;
}
