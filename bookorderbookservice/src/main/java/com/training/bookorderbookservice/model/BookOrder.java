package com.training.bookorderbookservice.model;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookOrder {
	private long orderId;
	private long bookId;
	private int totalPrice;
	private Date orderDate;
}
