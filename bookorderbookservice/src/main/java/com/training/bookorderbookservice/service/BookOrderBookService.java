package com.training.bookorderbookservice.service;

import java.util.List;

import com.training.bookorderbookservice.model.BookOrder;
import com.training.bookorderbookservice.model.Book;

public interface BookOrderBookService {

	List<Book> getAllBooks();

	Book createBookOrder(Book book);

	Book updateBookOrder(Book book);

	boolean deleteBookOrder(long bookid);

	//public List<Book> getEmployees();
	//public Book addEmployeeAddress(Book employee);
}
