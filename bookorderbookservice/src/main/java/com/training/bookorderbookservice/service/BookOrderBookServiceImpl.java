package com.training.bookorderbookservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.training.bookorderbookservice.model.BookOrder;
import com.training.bookorderbookservice.model.Book;

@Service
public class BookOrderBookServiceImpl implements BookOrderBookService {

	@Autowired
	BookWebService bookWebSevice;

	@Autowired
	BookOrderWebService bookorderWebService;

	@Override
	
	public List<Book> getAllBooks() {
		List<Book> bookList = bookWebSevice.getAllBooks();
		System.out.println();
		for (Book book : bookList) {
			//employee.setAddress(getAddress(employee));
			book.setBookorder(getBookOrder(book));
			
		}
		return bookList;
	}
	
	//@HystrixCommand(fallbackMethod = "defaultAddress")
	public BookOrder getBookOrder(Book book) {
		return bookorderWebService.getBookOrderByBookId(book.getBookId());
	}

	@Override
	public Book createBookOrder(Book book) {
		book.setBookorder(bookorderWebService.createBookOrder(book.getBookorder()));
		return book;
	}

	@Override
	public Book updateBookOrder(Book book) {
		book.setBookorder(bookorderWebService.updateBookOrder(book.getBookorder()));
		return book;
	}

	@Override
	public boolean deleteBookOrder(long bookid) {
		bookorderWebService.deleteBookOrderById(bookid);
		return true;
		
		
	}
	
	
	
	//public BookOrder defaultAddress(Book employee) {
		//return BookOrder.builder().state("TN").street("HCL Street").city("CN").country("IN").id(-1).employeeId(-1).pincode("-1").build();
	//}

	//@Override
	//public Book addEmployeeAddress(Book employee) {
		//employee.setAddress(addressWebService.addEmployee(employee.getAddress()));
		//return employee;
	//}

}
