package com.training.bookorderbookservice.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.training.bookorderbookservice.model.Book;


@FeignClient(value = "bookservice", url="http://localhost:1234")
public interface BookWebService {

	@GetMapping("/books")
	List<Book> getAllBooks();
	
	@PostMapping("/book")
	public Book createBook(Book book);
	
	@PutMapping("/book")
	public Book updateBook(Book book);
	
	@DeleteMapping("/book/{bookid}")
	public boolean deleteBook(long bookid);
}
