package com.training.bookorderbookservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.training.bookorderbookservice.model.BookOrder;


@FeignClient(value = "bookorderservice", url="http://localhost:4321")
public interface BookOrderWebService {
	
	//@GetMapping("/address/emp/{employeeId}")
	//@HystrixCommand(fallbackMethod = "getAddressByEmployeeId_Fallback")
	//public BookOrder getAddressByEmployeeId(@PathVariable long employeeId);
	
	//public static BookOrder getAddressByEmployeeId_Fallback(@PathVariable long employeeId) {
		//return new BookOrder();
	//}	
	
	//@PostMapping("/address")
	//public BookOrder addEmployee(BookOrder address);
	
	
	
	@GetMapping("bookorder/{bookId}")
	public BookOrder getBookOrderByBookId(@PathVariable long bookId);
	
	@PostMapping("/bookorder")
	public BookOrder createBookOrder(BookOrder BookOrder);
	
	@PutMapping("/bookorder")
	public BookOrder updateBookOrder(BookOrder bookorder);
	
	
	@DeleteMapping("/bookorder/{bookid}")
	public boolean deleteBookOrderById(long bookid);
}
