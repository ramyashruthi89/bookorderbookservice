package com.training.bookorderbookservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.training.bookorderbookservice.model.BookOrder;
import com.training.bookorderbookservice.model.Book;
import com.training.bookorderbookservice.service.BookOrderBookService;

@RestController
public class BookOrderBookController {
	
	@Autowired
	BookOrderBookService bookorderService;
	
	@GetMapping("/books")
	public List<Book> getAllBooks(){
		return bookorderService.getAllBooks();
	}
	
	@PostMapping("/bookorder")
	public Book createBookOrder(@RequestBody Book book) {
		return bookorderService.createBookOrder(book);
	}
	
	@PutMapping(value = "/bookorder")
	public Book updateBookOrder(@RequestBody Book book) {
		return bookorderService.updateBookOrder(book);
	}
	
	@DeleteMapping(value = "/bookorder/{bookid}")
	public String deleteBookOrderById(@PathVariable long bookid) {
		try {
			if (true) { 
				bookorderService.deleteBookOrder(bookid);
				return "Deleted Successfully";
			}
		} catch (Exception e) {
		}
		return "Cannot able to delete the BookOrder";

	}
	
}
