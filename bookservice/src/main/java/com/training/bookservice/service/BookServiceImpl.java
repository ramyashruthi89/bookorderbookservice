package com.training.bookservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.bookservice.model.Book;
import com.training.bookservice.repository.BookRepository;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	BookRepository repository;

	@Override
	public List<Book> getAllBooks() {
		return repository.findAll();
	}

	@Override
	public Book getOneBook(long id) {
		return repository.findById(id).orElse(new Book());
	}

	@Override
	public Book getOneBookByID(long bookid) {
		return repository.findById(bookid).orElse(new Book());
	
	}

	@Override
	public Book updateBook(Book book) {
		return repository.save(book);
		
	}

	@Override
	public boolean deleteBook(long bookid) {
		repository.deleteById(bookid);
		return true;
		
	}

	@Override
	public Book createBook(Book book) {
		return repository.save(book);
		
	}
	
	
	
	
}
