package com.training.bookservice.service;

import java.util.List;

import com.training.bookservice.model.Book;

public interface BookService {

	List<Book> getAllBooks();

	Book getOneBook(long id);

	Book getOneBookByID(long bookid);

	Book updateBook(Book book);

	boolean deleteBook(long bookid);

	Book createBook(Book book);
	
}
