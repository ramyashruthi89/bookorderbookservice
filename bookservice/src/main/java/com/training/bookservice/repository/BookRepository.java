package com.training.bookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.bookservice.model.Book;
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
