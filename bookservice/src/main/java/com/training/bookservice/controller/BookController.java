 package com.training.bookservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.bookservice.model.Book;
import com.training.bookservice.service.BookService;

@RestController
public class BookController {

	@Autowired
	BookService bookService;

	// CRUD
	// RequestMapping
	// Create --> PostMapping - completed
	// Read One--> GetMapping - completed
	// Read All--> GetMapping - completed
	// Update --> PutMapping - completed
	// Delete --> DeleteMapping - completed

	@GetMapping(value = "/books")
	//http://localhost:1234/books
	public List<Book> getAllBooks() {
		return bookService.getAllBooks();
	}

	@GetMapping(value = "/book")
	// http://localhost:1234/book?id=10
	// http://localhost:1234/employee/get?id=10&firstName=A - multiple variables
	public Book getOneBook(@RequestParam long id) {
		return  bookService.getOneBook(id);
	}

	@GetMapping(value = "/book/{bookid}")
	// http://localhost:1234/book/2
	// do not give too many path variables
	public Book getOneBookeById(@PathVariable long bookid ){
		return  bookService.getOneBookByID(bookid);
	}

	@PostMapping(value = "/book")
	public Book createBook(@RequestBody Book book) {
		return bookService.createBook(book);
	}

	@PutMapping(value = "/book")
	public Book updateBook(@RequestBody Book book) {
		System.out.println("Update the employee:" + book);
		return bookService.updateBook(book);
	}

	@DeleteMapping(value = "/book/{bookid}")
	public String deleteBookById(@PathVariable long bookid) {
		try {
			if (true) { 
				bookService.deleteBook(bookid);
				return "Deleted Successfully";
			}
		} catch (Exception e) {
		}
		return "Cannot able to delete the book";

	}
}
